//
//  MListViewController.m
//  Shoppy
//
//  Created by Manuel on 17/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "MListViewController.h"
#import "MShoppingItem.h"
#import "MAddItemViewController.h"
#import "MEditItemViewController.h"

@interface MListViewController ()
@property (nonatomic, strong) NSMutableArray *items;
@end

@implementation MListViewController

static NSString *addItemToShoppingListNotificationName = @"MShoppingListDidChangeNotification";

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title = [NSString stringWithFormat:NSLocalizedString(@"ItemsNavigationBarTitle", nil)];
        // Loading items
        [self loadItems];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addItem:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editItems:)];
    self.navigationItem.rightBarButtonItem.title = [NSString stringWithFormat:NSLocalizedString(@"EditButtonText", nil)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Edit items delegate method

-(void)controller:(MEditItemViewController *)controller didUpdateItem:(MShoppingItem *)item {
    // Fetch item
    for (int i = 0; i < [self.items count]; i++) {
        MShoppingItem *anItem = [self.items objectAtIndex:i];
        if ([[anItem uuid] isEqualToString:[item uuid]]) {
            // Updating table view row
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
    
    // Saving items
    [self saveItems];
}

#pragma mark -
#pragma mark - Data manipulation methods

-(void)addItem:(id)sender {
    MAddItemViewController *addItemViewController = [[MAddItemViewController alloc] initWithNibName:@"MAddItemViewController" bundle:nil];
    [addItemViewController setDelegate:self];
    [self presentViewController:addItemViewController animated:YES completion:nil];
}

-(void)editItems:(id)sender {
    [self.tableView setEditing:![self.tableView isEditing] animated:YES];
}

#pragma mark -
#pragma mark - Delegate methods implementation

-(void)controller:(MAddItemViewController *)controller didSaveItemWithName:(NSString *)name andPrice:(float)price {
    MShoppingItem *item = [MShoppingItem createShoppingItemWithName:name andPrice:price];
    [self.items addObject:item];
    // Adding rows to the table view
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:([self.items count]-1) inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - 
#pragma mark - Auxiliary methods for archiving/unarchiving

-(void)loadItems {
    NSString *filePath = [self pathForItems];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        self.items = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    } else {
        self.items = [NSMutableArray array];
    }
}

-(void)saveItems {
    NSString *filepath = [self pathForItems];
    [NSKeyedArchiver archiveRootObject:self.items toFile:filepath];
    // Post notification
    [[NSNotificationCenter defaultCenter] postNotificationName:addItemToShoppingListNotificationName object:self];

}

-(NSString *)pathForItems {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [paths lastObject];
    return [documents stringByAppendingPathComponent:@"items.plist"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.items count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
        [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    // Configure the cell...
    MShoppingItem *item = [self.items objectAtIndex:[indexPath row]];
    [cell.textLabel setText:[item name]];
    [cell setAccessoryType:UITableViewCellAccessoryDetailDisclosureButton];
    // Show / hide checkmark
    if ([item inShoppingList]) {
        [cell.imageView setImage:[UIImage imageNamed:@"checkmark"]];
    } else {
        [cell.imageView setImage:nil];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    // Fetch item
    MShoppingItem *item = [self.items objectAtIndex:[indexPath row]];
    // Initialize controller
    MEditItemViewController *controller = [[MEditItemViewController alloc] initWithItem:item andDelegate:self];
    // Pushing controller onto stack
    [self.navigationController pushViewController:controller animated:YES];
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the items from the containing list
        [self.items removeObjectAtIndex:indexPath.row];
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self saveItems];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // Fetch Item
    MShoppingItem *item = [self.items objectAtIndex:[indexPath row]];
    // Update Item
    [item setInShoppingList:![item inShoppingList]];
    // Update Cell
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([item inShoppingList]) {
        [cell.imageView setImage:[UIImage imageNamed:@"checkmark"]];
    } else {
        [cell.imageView setImage:nil];
    }
    // Save Items
    [self saveItems];
}

@end
