//
//  MShoppingItem.m
//  Shoppy
//
//  Created by Manuel on 17/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "MShoppingItem.h"

@implementation MShoppingItem

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        [self setUuid:[aDecoder decodeObjectForKey:@"uuid"]];
        [self setName:[aDecoder decodeObjectForKey:@"name"]];
        [self setPrice:[aDecoder decodeFloatForKey:@"price"]];
        [self setInShoppingList:[aDecoder decodeBoolForKey:@"inShoppingList"]];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.uuid forKey:@"uuid"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeFloat:self.price forKey:@"price"];
    [aCoder encodeBool:self.inShoppingList forKey:@"inShoppingList"];
}

// Optional

+(MShoppingItem *)createShoppingItemWithName:(NSString *)name andPrice:(float)price {
    // Initializing item
    MShoppingItem *item = [[MShoppingItem alloc] init];
    // Configuring item
    [item setName:name];
    [item setPrice:price];
    [item setIsAccessibilityElement:NO];
    [item setUuid:[[NSUUID UUID] UUIDString]];
    return item;
}

@end
