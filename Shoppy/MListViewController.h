//
//  MListViewController.h
//  Shoppy
//
//  Created by Manuel on 17/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAddItemViewController.h"
#import "MEditItemViewController.h"

@interface MListViewController : UITableViewController<MAddItemViewControllerDelegate, MEditItemViewControllerDelegate>

@end
