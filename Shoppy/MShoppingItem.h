//
//  MShoppingItem.h
//  Shoppy
//
//  Created by Manuel on 17/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MShoppingItem : NSObject<NSCoding>
@property NSString *uuid;
@property NSString *name;
@property float price;
@property BOOL inShoppingList;

+(MShoppingItem *)createShoppingItemWithName:(NSString *)name andPrice:(float)price;

@end
