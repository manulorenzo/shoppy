//
//  MEditItemViewController.h
//  Shoppy
//
//  Created by manu on 22/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MShoppingItem.h"

@protocol MEditItemViewControllerDelegate;
@interface MEditItemViewController : UIViewController
@property IBOutlet UITextField *nameTextField;
@property IBOutlet UITextField *priceTextField;

#pragma mark -
#pragma mark Initialization

-(id)initWithItem:(MShoppingItem *)item andDelegate:(id<MEditItemViewControllerDelegate>) delegate;
@end

@protocol MEditItemViewControllerDelegate <NSObject>
-(void)controller:(MEditItemViewController *)controller didUpdateItem:(MShoppingItem *)item;
@end

