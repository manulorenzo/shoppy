//
//  MEditItemViewController.m
//  Shoppy
//
//  Created by manu on 22/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import "MEditItemViewController.h"
#import "MShoppingItem.h"
@interface MEditItemViewController ()
@property MShoppingItem *item;
@property (weak) id<MEditItemViewControllerDelegate> delegate;
@end

@implementation MEditItemViewController

-(id)initWithItem:(MShoppingItem *)item andDelegate:(id<MEditItemViewControllerDelegate>)delegate {
    self = [super initWithNibName:@"MEditItemViewController" bundle:nil];
    if (self) {
        // Set item
        self.item = item;
        // Set delegate
        self.delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveItems:)];
    
    // Populate text fields
    if (self.item) {
        [self.nameTextField setText:[self.item name]];
        [self.priceTextField setText:[NSString stringWithFormat:@"%f", [self.item price]]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)saveItems:(id)sender {
    NSString *name = [self.nameTextField text];
    float price = [[self.priceTextField text] floatValue];
    // Update item
    [self.item setName:name];
    [self.item setPrice:price];
    // Notify delegate
    [self.delegate controller:self didUpdateItem:self.item];
    // Pop view controller
    [self.navigationController popViewControllerAnimated:YES];
}

@end
