//
//  MAddItemViewController.h
//  Shoppy
//
//  Created by Manuel on 21/05/2013.
//  Copyright (c) 2013 Manuel Lorenzo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MAddItemViewControllerDelegate;
@interface MAddItemViewController : UIViewController
// Private delegate!
@property (weak) id<MAddItemViewControllerDelegate> delegate;
@property IBOutlet UITextField *nameTextField;
@property IBOutlet UITextField *priceTextField;
@end

@protocol MAddItemViewControllerDelegate <NSObject>
-(void)controller:(MAddItemViewController *)controller didSaveItemWithName:(NSString *)name andPrice:(float)price;
@end
