Shoppy
======
Simple app to help manage shopping lists.

Features:
- Allows the users to add/remove items and mark them as already bought.
- Set of default products which will appear when the app is first started up.
- Creation of shopping lists.

Features to be done in the future:
- Ability to remove items straight from the shopping list via single tap.

